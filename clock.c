#define F_CPU 8000000UL

#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/io.h>
#include "D:\ProgsIII\myLib\I2c\L2C-loc.h"




#define SREG _SFR_IO8(0x3F)

#define INIT_I2C
#define WRITE_I2C


#define DS3231 0b11010000
#define BUTFORWRITE 6
#define BUTMIN 2
#define BUTHOUR 4
#define BUTLEADUP 5

#define ADDSECDS3231 0x00

#define B_W    0b00000000
#define B_R    0b00000001

char  sec = 0; char min = 0; char hour = 0;
char addsec;
char Writemod =0;

char Arrfs[2];
char Arrfg[2];

//---------------------------0-----1-----2-----3-----4-----5-----6-----7-----8------9----------------сегменты!!!
char SEGMENTE[] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F};
char segcounter = 0;

// Обработчик прерывания по переполнению таймера 0
ISR(TIMER0_OVF_vect)
{
	PORTB = 0x00; // Гасим все разряды
	PORTD |=(1<<PD0) ;
	PORTD &= ~(1<<PD0) ;

	switch(segcounter)
	{
		case 0:
		PORTB |=(SEGMENTE[hour % 10]+0b10000000); // Раскладываем число на разряды
		break;

		case 1:
		PORTB |=(SEGMENTE[min % 100/10] );
		break;

		case 2:
		PORTB |=(SEGMENTE[min % 10]+0b10000000);
		break;

		case 3:
		PORTB |=(SEGMENTE[sec % 100/10 ]);
		break;

		case 4:
		PORTB |=(SEGMENTE[sec % 10]);
		break;

		case 5:
		PORTB |=(SEGMENTE[hour %100/10]);
		break;
	}
	segcounter++;
	if(segcounter > 5) {segcounter = 0;}
}
//----------------------------------------------------------------------------- модуль сегментов
//-----------------------------------------------------------------------------  инициализация часов,
void intclk ()
{
	GICR |=(1<<INT1);// Разрешаем прерывание INT1
	MCUCR &= ~(0<<ISC11)|(0<<ISC10);// Генерация сигнала при низком уровне на ножке INT1
	asm ("sei");//Разрешить прерывания

	char confclc[] = {0b00000000};  // обнуление регистра с настройками часов, что б тактировал
	_write_m(DS3231,0x0E,0,confclc);

	_read_m(DS3231,0x00,2,Arrfg); //  синхронизация часов и avr , при включении avr

	sec= Arrfg[0];
	min= Arrfg[1];
	hour=Arrfg[2];
}
//----------------------------------------------------------------------------- счетный механизм!!!
ISR(INT1_vect)
{
	sec++ ;
	if (sec >= 60)    {min++; sec=0;}
	if (min >= 60)    {hour++; min=0; sec=0;}
	if (hour >= 24)   {hour= 0; min =0; sec= 0;}
}
//-----------------------------------------------------------------------------
void main(void)
{

	intclk();

	TIMSK |= (1 << TOIE0)  |(1<<OCIE1A)  ;		   // Разрешение прерывания по таймеру 0 по переполнению | разрешение внешнего прерывания таймера 1
	TCCR0  |= (0<<CS02) |(0 <<CS01) |(1<<CS00);    // таймер для настройки индикаторов
	OCR1A  = 15625;
	//----------------------------

	DDRC=0xFF;
	DDRB=0xFF;
	DDRD=0x00;
	DDRD |= (1<<PD0)|(1<<PD1);


	init_DS1307();

	while (1)
	{
		PORTD |= (1<<BUTFORWRITE)|(1<<BUTHOUR)|(1<<BUTMIN);

		//---------------------------------------------------- обработчик кнопок
		//---------------------------ВКЛ-------------------------
		while ((PIND &(1<<BUTFORWRITE))==0)   //------------тумблер записи  ( включен - ввод значений, выключен - отправка )
		{
			Writemod=1;

			if ((PIND &(1<<BUTMIN))==0)
			{
				min++;
				if (min>=60)
				{min = 0;}
				_delay_ms (200);
			}
			if ((PIND &(1<<BUTHOUR))==0)
			{
				hour++;
				if (hour>= 24)
				{hour = 0;}
				_delay_ms (200);
			}
		}
		//---------------------------ВКЛ--------------------------
		//---------------------------ВЫКЛ-------------------------
		if (Writemod == 1)
		{
			Writemod= 0;
			// готовим массив для отправки
			Arrfs[0]=0x00;     //0x00;
			Arrfs[1]=min;     //min;
			Arrfs[2]=hour;   //hour;

			_write_m(DS3231,ADDSECDS3231,2,Arrfs); // запись в ДС, в первую ячейку памяти(секунды), 3 пакетов(0-1-2), из массива Arrfs
			_read_m(DS3231,ADDSECDS3231,2,Arrfg);

			sec= Arrfg[0];
			min= Arrfg[1];
			hour=Arrfg[2];

		}
		//---------------------------ВЫКЛ-------------------------

	}


}
