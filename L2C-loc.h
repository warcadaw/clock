#ifndef L2CLOC_H_INCLUDED
#define L2CLOC_H_INCLUDED


#define INIT_I2C
#define WRITE_I2C
#define READ_I2C

#include <avr/io.h>
#include <avr/interrupt.h>

//********** Настройка портов

#define PIN_i2c  PINC
#define PORT_i2c PORTC
#define DDR_i2c  DDRC
#define SDA      PORTC4
#define SCL      PORTC5

//********** Состояния регистра TWSR
#define TW_START                    0x08  //условие СТАРТ
#define TW_REP_START                0x10  //условие ПОВТОРНЫЙ СТАРТ (повтор условия начала передачи)
// Master Transmitter (Ведущий в роли передающего)
#define TW_MT_SLA_ACK               0x18  //Ведущий отправил адрес и бит записи. Ведомый подтвердил свой адрес
#define TW_MT_SLA_NACK              0x20  //Ведущий отправил адрес и бит записи. Нет подтверждения приема (ведомый с таким адресом не найден)
#define TW_MT_DATA_ACK              0x28  //Ведущий передал данные и ведомый подтвердил прием.
#define TW_MT_DATA_NACK             0x30  //Ведущий передал данные. Нет подтверждения приема
#define TW_MT_ARB_LOST              0x38  //Потеря приоритета
// Master Receiver (Ведущий в роли принимающего)
#define TW_MR_ARB_LOST              0x38  //Потеря приоритета
#define TW_MR_SLA_ACK               0x40  // Ведущий отправил адрес и бит чтения. Ведомый подтвердил свой адрес
#define TW_MR_SLA_NACK              0x48  //Ведущий отправил адрес и бит чтения. Нет подтверждения приема (ведомый с таким адресом не найден)
#define TW_MR_DATA_ACK              0x50  //Ведущий принял данные и передал подтверждение
#define TW_MR_DATA_NACK             0x58  //Ведущий принял данные и не передал подтверждение
// Slave Transmitter (Ведомый в роли передающего)
#define TW_ST_SLA_ACK               0xA8  //Получение адреса и бита чтения, возвращение подтверждения
#define TW_ST_ARB_LOST_SLA_ACK      0xB0  //Потеря приоритета, получение адреса и бита чтения, возвращение подтверждения
#define TW_ST_DATA_ACK              0xB8  //Данные переданы, подтверждение от ведущего принято
#define TW_ST_DATA_NACK             0xC0  //Данные переданы. Нет подтверждения приема от ведущего.
#define TW_ST_LAST_DATA             0xC8  //Последний переданный байт данных, получение подтверждения
// Slave Receiver (Ведомый в роли принимающего)
#define TW_SR_SLA_ACK               0x60  //Получение адреса и бита записи, возвращение подтверждения
#define TW_SR_ARB_LOST_SLA_ACK      0x68  //Потеря приоритета, получение адреса и бита записи, возвращение подтверждения
#define TW_SR_GCALL_ACK             0x70  //Прием общего запроса, возвращение подтверждения
#define TW_SR_ARB_LOST_GCALL_ACK        0x78  //Потеря приоритета, прием общего запроса, возвращение подтверждения
#define TW_SR_DATA_ACK              0x80  // Прием данных, возвращение подтверждения
#define TW_SR_DATA_NACK             0x88  //Данные переданы без подтверждения.
#define TW_SR_GCALL_DATA_ACK            0x90  //Прием данных при общем запросе, возвращение подтверждения
#define TW_SR_GCALL_DATA_NACK           0x98  // Прием данных при общем запросе, без подтверждения
#define TW_SR_STOP                  0xA0  //Условие СТОП
// Misc (Ошибки интерфейса)
#define TW_NO_INFO                      0xF8  //Информация о состоянии отсутствует
#define TW_BUS_ERROR                0x00  //Ошибка шины

//********** значения адресов устройств
//#define DS3231 0b11010000


#ifdef INIT_I2C

void init_DS1307 (void)
{
TWBR = 27; /*При частоте 8 МГц */
TWSR |= (0 << TWPS1)|(0 << TWPS0); /*Пред делитель на 0*/
TWCR |= (1 << TWEN); /*Включение модуля TWI*/
}


#endif // INIT_I2C


/* функция передачи адреса памяти ячейки и пакетов */

#ifdef WRITE_I2C
void _write_m (char slaveAddr, char addPACK, char num_pack, char *wPACK)
{

char g;

		//-------- START
	    TWCR=(1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
	    while(!(TWCR & (1<<TWINT))); // ожидания освобождения шины
//	    if((TWSR & 0xF8) != TW_START);

		//------- SEND ADDRESS
	    char sendaddr = slaveAddr ;
	    TWDR = sendaddr;
	    TWCR =(1<<TWINT|1<<TWEN);
	    while(!(TWCR & (1<<TWINT)));
  //      if ((TWSR & 0xF8) != TW_MT_SLA_ACK);

		//------- SEND ADDRESS BYTE
		TWDR = addPACK;
		TWCR =(1<<TWINT)|(1<<TWEN);
		while(!(TWCR & (1<<TWINT)));
 //       if ((TWSR & 0xF8) != TW_MT_DATA_ACK);
        //------- SEND DATA-PACK
            for (g=0;g<=num_pack;g++)       //0 < 1 -- цикл идет
            {
				TWDR= wPACK[g];
				TWCR =(1<<TWINT|1<<TWEN);
				while(!(TWCR & (1<<TWINT)));
	//			if ((TWSR & 0xF8) != TW_MT_DATA_ACK);
            }
		//------ STOP
	    TWCR =(1<<TWINT)|(1<<TWSTO)|(1<<TWEN);
return ;
}

void _read_m (char slaveAddr, char addPACK, char num_pack, char *wPACK)
{

char g;
         //------------------------------------------------SEND POSITION OF ADD COUNTER (ADDRESS BYTE)
		//-------- START
	    TWCR=(1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
	    while(!(TWCR & (1<<TWINT))); // ожидания освобождения шины
//	    if((TWSR & 0xF8) != TW_START);

		//------- SEND ADDRESS
	    char sendaddr = slaveAddr + 0b00000000 ;
	    TWDR = sendaddr;
	    TWCR =(1<<TWINT|1<<TWEN);
	    while(!(TWCR & (1<<TWINT)));
  //      if ((TWSR & 0xF8) != TW_MT_SLA_ACK);

  		//------- SEND ADDRESS BYTE
		TWDR = addPACK;
		TWCR =(1<<TWINT)|(1<<TWEN);
		while(!(TWCR & (1<<TWINT)));
 //       if ((TWSR & 0xF8) != TW_MT_DATA_ACK);

 		//------ STOP
	    TWCR =(1<<TWINT)|(1<<TWSTO)|(1<<TWEN);
        //------------------------------------------------- END ------------------------------------

  		//-------- START
	    TWCR=(1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
	    while(!(TWCR & (1<<TWINT))); // ожидания освобождения шины
//	    if((TWSR & 0xF8) != TW_START);

		//------- SEND ADDRESS
        sendaddr = slaveAddr + 0b00000001 ;
	    TWDR = sendaddr;
	    TWCR =(1<<TWINT)|(1<<TWEN)|(1<<TWEA);
	    while(!(TWCR & (1<<TWINT)));
  //      if ((TWSR & 0xF8) != TW_MT_SLA_ACK);

        //------- ACCEPT DATA
			for ( g=0; g<=num_pack; g++)
			{
			    if (g<num_pack) {TWCR =(1<<TWINT)|(1<<TWEN)|(1<<TWEA);}
			    else {TWCR =(1<<TWINT)|(1<<TWEN);}
			    asm ("cli");
				while(!(TWCR & (1<<TWINT)));
    //          if ((TWSR & 0xF8) != TW_MT_DATA_NACK);
                wPACK[g] =TWDR;
                asm ("sei");
				
			}

		//------ STOP
	    TWCR =(1<<TWINT)|(1<<TWSTO)|(1<<TWEN);

return;
}
#endif //

#endif // L2C-LOC_H_INCLUDED
